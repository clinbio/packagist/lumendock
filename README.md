# Lumendock ![version](https://img.shields.io/badge/version-v1.0.0-blue)

Minimalistic Lumen (PHP framework) development environment for Docker. This package is a fork of: https://github.com/shipping-docker/vessel for Lumen use.

## Install

Lumendock is just a small set of files that sets up a local Docker-based dev environment per lumen projects. There is nothing to install globally, except Docker itself!

This is all there is to using it:

```bash
composer require dterumal/lumendock:1.0.0 
cp -R vendor/dterumal/lumendock/docker-files/{lumendock,docker-compose.yml,docker} .

# Run this once to initialize project
# Must run with "bash" until initialized
bash lumendock init

./lumendock start
```

Head to `http://localhost` in your browser and see your Lumen site!

## Common Commands

Here's a list of built-in helpers you can use. Any command not defined in the `lumendock` script will default to being passed to the `docker-compose` command. If not command is used, it will run `docker-compose ps` to list the running containers for this environment.

### Show Lumendock Version or Help

```bash
# shows lumendock current version
$ lumendock --version # or [ -v | version ]

# shows lumendock help
$ lumendock --help # or [ -H | help ]
```

### Starting and Stopping Lumendock

```bash
# Start the environment
./lumendock start

## This is equivalent to
./lumendock up -d

# Stop the environment
./lumendock stop

## This is equivalent to
./lumendock down
```

### Development

```bash
# Use artisan
./lumendock artisan <cmd>
./lumendock art <cmd> # "art" is a shortcut to "artisan"
```

## What's included?

The aim of this project is simplicity. It includes:

* Nginx 1.18-alpine
* PHP 7.2-fpm-alpine
* MySQL 5.7
* NodeJS 6-alpine

## Supported Systems

Lumendock requires Docker, and currently only works on Windows, Mac and Linux.

> Windows requires running Hyper-V.  Using Git Bash (MINGW64) and WSL are supported.  Native
  Windows is still under development.

| Mac                                                                      |                                              Linux                                              |                                     Windows                                      |
| ------------------------------------------------------------------------ | :---------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------: |
| Install Docker on [Mac](https://docs.docker.com/docker-for-mac/install/) | Install Docker on [Debian](https://docs.docker.com/engine/installation/linux/docker-ce/debian/) | Install Docker on [Windows](https://docs.docker.com/docker-for-windows/install/) |
|                                                                          | Install Docker on [Ubuntu](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/) |                                                                                  |
|                                                                          | Install Docker on [CentOS](https://docs.docker.com/engine/installation/linux/docker-ce/centos/) |                                                                                  |
