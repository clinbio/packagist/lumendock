<?php

namespace Lumendock;

use Illuminate\Support\ServiceProvider;

class LumendockServiceProvider extends ServiceProvider {

    public function boot()
    {
        $this->publishes([
            LUMENDOCK_PATH.'/docker-files/docker' => base_path('docker'),
            LUMENDOCK_PATH.'/docker-files/docker-compose.yml' => base_path('docker-compose.yml'),
            LUMENDOCK_PATH.'/docker-files/lumendock' => base_path('lumendock'),
        ]);
    }

    public function register()
    {
        if (! defined('LUMENDOCK_PATH')) {
            define('LUMENDOCK_PATH', realpath(__DIR__.'/../'));
        }
    }

}